### Hi there, I'm Kasun - aka [kasuncfdo] <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> ![](https://visitor-badge.glitch.me/badge?page_id=kasuncfdo.kasuncfdo)


<a href="https://www.instagram.com/kasun_c_fdo__/">
  <img align="left" alt="Abhishek's Discord" width="22px" src="https://raw.githubusercontent.com/peterthehan/peterthehan/master/assets/discord.svg" />
</a>
<a href="https://twitter.com/kasuncfdo">
  <img align="left" alt="Abhishek Naidu | Twitter" width="22px" src="https://raw.githubusercontent.com/peterthehan/peterthehan/master/assets/twitter.svg" />
</a>
<a href="https://lk.linkedin.com/in/kcfernando">
  <img align="left" alt="Abhishek's LinkedIN" width="22px" src="https://raw.githubusercontent.com/peterthehan/peterthehan/master/assets/linkedin.svg" />
</a>
<a href="https://open.spotify.com/user/5c6aj47pk1n9c96yr85dyqkvz">
  <img align="left" alt="Abhishek's Spotify" width="22px" src="https://raw.githubusercontent.com/peterthehan/peterthehan/master/assets/spotify.svg" />
</a>

[![Twitter Follow](https://img.shields.io/twitter/follow/kasuncfdo?color=1DA1F2&logo=twitter&style=for-the-badge)](https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fgithub.com%2Fkasuncfdo&screen_name=kasuncfdo)

## I'm a Designer, Tech Enthusiast, Developer, and Content Creater!!

- 🔭 I'm Kasun
- 🌱 I’m currently learning everything 🤣
- 👯 I’m looking to collaborate with other content creators
- 🥅 2021 Goals: Contribute more to Open Source projects
- ⚡ Fun fact: I love to Think Different

### Spotify Playing 🎧

[![Spotify](https://novatorem-kasuncfdo.vercel.app/api/spotify)](https://open.spotify.com/user/5c6aj47pk1n9c96yr85dyqkvz)

### Connect with me:
<p>I am also an open-source enthusiast and maintainer. I learned a lot from the open-source community and I love how collaboration and knowledge sharing happened through open-source.</p>
<a href="https://www.instagram.com/kasun_c_fdo__/">
   <img align="left" alt="codeSTACKr.com" width="22px" src="https://github.com/gauravghongde/social-icons/blob/master/SVG/Color/Facebook.svg" />
</a>
<a href="https://www.facebook.com/kasunchanukafernando">
<img align="left" alt="codeSTACKr | Fb" width="22px" src="https://github.com/gauravghongde/social-icons/blob/master/SVG/Color/Instagram.svg" />
</a>
<a href="https://twitter.com/kasuncfdo">
<img align="left" alt="kasuncfdo | Twitter" width="22px" src="https://github.com/gauravghongde/social-icons/blob/master/SVG/Color/Twitter.svg" />
</a>


<br />

### ⚡ GitHub Stats:
![GitHub Stats](https://github-readme-stats.vercel.app/api?username=kasuncfdo&show_icons=true&theme=gotham)


<br />
<br />

---

<details>
  
  <summary>:zap: GitHub Stats</summary>
  
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=kasuncfdo)](https://github.com/anuraghazra/github-readme-stats)  
  
 ![GitHub streak stats](https://github-readme-streak-stats.herokuapp.com/?user=kasuncfdo)   
  
 ![GitHub Stats](https://github-readme-stats.vercel.app/api?username=kasuncfdo&theme=radical) 
  
![GitHub Activity Graph](https://activity-graph.herokuapp.com/graph?username=kasuncfdo)  
  
  <p> Grow in ❤ with Technology </p>
  


</details>

